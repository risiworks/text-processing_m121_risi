﻿// RISGESAN THAYAKARAN M121 TEXT-PROCESSING 16.12.2020
using System;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;
using System.IO;

namespace Text_Processing
{
    static class caluculator
    {
        public static void calculate(string file)
        {
            //  timer
            var timer = Stopwatch.StartNew();

            // filehandling
            var bs = new BufferedStream(File.OpenRead(file), 100_000);
            var sr = new StreamReader(bs);

            double solution = default;

            while (!sr.EndOfStream) solution += CalcLine(sr.ReadLine());
            timer.Stop();


            Console.WriteLine($"LÖSUNG: {solution}");
            Console.WriteLine($"ZEIT: {timer.Elapsed}");
        }

        private static double CalcLine(string Ziffer)
        {
            bool minusZahl = false;
            double Zahl = default;
            var zeichen = Zeichen.operation;
            double decimalZahl = 1;

            foreach (char f in Ziffer)
            switch (zeichen)
            {
                 // Vorzeichen
                case Zeichen.operation:
                if (f == '+' || f == '-'){
                    if (f == '-') minusZahl = true;
                    zeichen = Zeichen.ersteskomma;}
                else{
                    Zahl *= 10;
                    Zahl += f - '0';
                    zeichen = Zeichen.vorKomma;};
                break;

                //erste Komma
                case Zeichen.ersteskomma:
                if (char.IsDigit(f))
                {Zahl *= 10;
                    Zahl += f - '0';
                    zeichen = Zeichen.vorKomma;}
                break;

                //vor Komma
                case Zeichen.vorKomma:
                if (char.IsDigit(f)) {
                    Zahl *= 10;
                    Zahl += f - '0'; }
                else if (f == '.'){
                    zeichen = Zeichen.NachKomma; };
                break;

                // nach Komma
                case Zeichen.NachKomma:
                if (f != ','){
                    decimalZahl /= 10;
                    Zahl += (f - '0') * decimalZahl; }
                break;
            }
            if (minusZahl) Zahl *= -1;
            return Zahl;
        }
    }

    // enum
    public enum Zeichen
    {
        operation,
        ersteskomma,
        vorKomma,
        NachKomma
    }
}
